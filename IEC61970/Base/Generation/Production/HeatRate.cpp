///////////////////////////////////////////////////////////
//  HeatRate.cpp
//  Implementation of the Class HeatRate
//  Created on:      28-Jan-2016 12:45:17
///////////////////////////////////////////////////////////

#include "HeatRate.h"

using IEC61970::Base::Generation::Production::HeatRate;


HeatRate::HeatRate(){

}



HeatRate::~HeatRate(){

}


const IEC61970::Base::Domain::UnitSymbol HeatRate::unit = IEC61970::Base::Domain::UnitSymbol::J;
