///////////////////////////////////////////////////////////
//  HydroPowerPlant.cpp
//  Implementation of the Class HydroPowerPlant
//  Created on:      28-Jan-2016 12:45:20
///////////////////////////////////////////////////////////

#include "HydroPowerPlant.h"

using IEC61970::Base::Generation::Production::HydroPowerPlant;


HydroPowerPlant::HydroPowerPlant()
	: Reservoir(nullptr), GenSourcePumpDischargeReservoir(nullptr)
{

}



HydroPowerPlant::~HydroPowerPlant(){

}
