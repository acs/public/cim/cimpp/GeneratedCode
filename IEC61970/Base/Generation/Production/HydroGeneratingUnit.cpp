///////////////////////////////////////////////////////////
//  HydroGeneratingUnit.cpp
//  Implementation of the Class HydroGeneratingUnit
//  Created on:      28-Jan-2016 12:45:20
///////////////////////////////////////////////////////////

#include "HydroGeneratingUnit.h"

using IEC61970::Base::Generation::Production::HydroGeneratingUnit;


HydroGeneratingUnit::HydroGeneratingUnit()
	: PenstockLossCurve(nullptr)
{

}



HydroGeneratingUnit::~HydroGeneratingUnit(){

}
