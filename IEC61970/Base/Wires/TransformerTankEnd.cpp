///////////////////////////////////////////////////////////
//  TransformerTankEnd.cpp
//  Implementation of the Class TransformerTankEnd
//  Created on:      28-Jan-2016 12:47:25
//  Original author: T. Kostic
///////////////////////////////////////////////////////////

#include "TransformerTankEnd.h"

using IEC61970::Base::Wires::TransformerTankEnd;


TransformerTankEnd::TransformerTankEnd()
	: TransformerTank(nullptr)
{

}



TransformerTankEnd::~TransformerTankEnd(){

}
