///////////////////////////////////////////////////////////
//  TransformerMeshImpedance.cpp
//  Implementation of the Class TransformerMeshImpedance
//  Created on:      28-Jan-2016 12:47:23
//  Original author: LOO
///////////////////////////////////////////////////////////

#include "TransformerMeshImpedance.h"

using IEC61970::Base::Wires::TransformerMeshImpedance;


TransformerMeshImpedance::TransformerMeshImpedance()
	: FromTransformerEnd(nullptr)
{

}



TransformerMeshImpedance::~TransformerMeshImpedance(){

}
