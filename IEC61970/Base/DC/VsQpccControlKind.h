///////////////////////////////////////////////////////////
//  VsQpccControlKind.h
//  Implementation of the Class VsQpccControlKind
//  Created on:      28-Jan-2016 12:47:43
//  Original author: selaost1
///////////////////////////////////////////////////////////

#if !defined(EA_14B4BF99_1055_4bdf_9AFB_17479192119A__INCLUDED_)
#define EA_14B4BF99_1055_4bdf_9AFB_17479192119A__INCLUDED_

namespace IEC61970
{
	namespace Base
	{
		namespace DC
		{
			enum class VsQpccControlKind
			{
				reactivePcc,
				voltagePcc,
				powerFactorPcc
			};

		}

	}

}
#endif // !defined(EA_14B4BF99_1055_4bdf_9AFB_17479192119A__INCLUDED_)
