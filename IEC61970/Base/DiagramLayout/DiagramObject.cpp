///////////////////////////////////////////////////////////
//  DiagramObject.cpp
//  Implementation of the Class DiagramObject
//  Created on:      28-Jan-2016 12:44:04
//  Original author: mcmorran
///////////////////////////////////////////////////////////

#include "DiagramObject.h"

using IEC61970::Base::DiagramLayout::DiagramObject;


DiagramObject::DiagramObject()
	: Diagram(nullptr), DiagramObjectStyle(nullptr)
{

}



DiagramObject::~DiagramObject(){

}
