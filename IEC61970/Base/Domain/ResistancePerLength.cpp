///////////////////////////////////////////////////////////
//  ResistancePerLength.cpp
//  Implementation of the Class ResistancePerLength
//  Created on:      28-Jan-2016 12:46:35
//  Original author: T. Kostic
///////////////////////////////////////////////////////////

#include "ResistancePerLength.h"

using IEC61970::Base::Domain::ResistancePerLength;


ResistancePerLength::ResistancePerLength(){

}



ResistancePerLength::~ResistancePerLength(){

}


const IEC61970::Base::Domain::UnitMultiplier ResistancePerLength::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol ResistancePerLength::unit = IEC61970::Base::Domain::UnitSymbol::ohmPerm;
