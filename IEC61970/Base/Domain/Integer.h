///////////////////////////////////////////////////////////
//  Integer.h
//  Implementation of the Class Integer
//  Created on:      28-Jan-2016 12:45:29
///////////////////////////////////////////////////////////

#if !defined(EA_8B7A3026_33C5_404c_B28B_29F2613DF377__INCLUDED_)
#define EA_8B7A3026_33C5_404c_B28B_29F2613DF377__INCLUDED_

namespace IEC61970
{
	namespace Base
	{
		namespace Domain
		{
			/**
			 * An integer number. The range is unspecified and not limited.
			 */
			typedef long Integer;

		}

	}

}
#endif // !defined(EA_8B7A3026_33C5_404c_B28B_29F2613DF377__INCLUDED_)
