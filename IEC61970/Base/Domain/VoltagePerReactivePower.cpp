///////////////////////////////////////////////////////////
//  VoltagePerReactivePower.cpp
//  Implementation of the Class VoltagePerReactivePower
//  Created on:      28-Jan-2016 12:47:40
///////////////////////////////////////////////////////////

#include "VoltagePerReactivePower.h"

using IEC61970::Base::Domain::VoltagePerReactivePower;


VoltagePerReactivePower::VoltagePerReactivePower(){

}



VoltagePerReactivePower::~VoltagePerReactivePower(){

}


const IEC61970::Base::Domain::UnitSymbol VoltagePerReactivePower::unit = IEC61970::Base::Domain::UnitSymbol::VPerVAr;
