///////////////////////////////////////////////////////////
//  AngleRadians.cpp
//  Implementation of the Class AngleRadians
//  Created on:      28-Jan-2016 12:43:21
///////////////////////////////////////////////////////////

#include "AngleRadians.h"

using IEC61970::Base::Domain::AngleRadians;


AngleRadians::AngleRadians(){

}



AngleRadians::~AngleRadians(){

}


const IEC61970::Base::Domain::UnitMultiplier AngleRadians::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol AngleRadians::unit = IEC61970::Base::Domain::UnitSymbol::rad;
