///////////////////////////////////////////////////////////
//  Length.cpp
//  Implementation of the Class Length
//  Created on:      28-Jan-2016 12:45:32
///////////////////////////////////////////////////////////

#include "Length.h"

using IEC61970::Base::Domain::Length;


Length::Length(){

}



Length::~Length(){

}


const IEC61970::Base::Domain::UnitSymbol Length::unit = IEC61970::Base::Domain::UnitSymbol::m;
