///////////////////////////////////////////////////////////
//  ReactivePower.cpp
//  Implementation of the Class ReactivePower
//  Created on:      28-Jan-2016 12:46:26
///////////////////////////////////////////////////////////

#include "ReactivePower.h"

using IEC61970::Base::Domain::ReactivePower;


ReactivePower::ReactivePower(){

}



ReactivePower::~ReactivePower(){

}


const IEC61970::Base::Domain::UnitSymbol ReactivePower::unit = IEC61970::Base::Domain::UnitSymbol::VAr;
