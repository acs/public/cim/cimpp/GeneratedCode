///////////////////////////////////////////////////////////
//  Admittance.cpp
//  Implementation of the Class Admittance
//  Created on:      28-Jan-2016 12:43:15
///////////////////////////////////////////////////////////

#include "Admittance.h"

using IEC61970::Base::Domain::Admittance;


Admittance::Admittance(){

}



Admittance::~Admittance(){

}


const IEC61970::Base::Domain::UnitSymbol Admittance::unit = IEC61970::Base::Domain::UnitSymbol::S;
