///////////////////////////////////////////////////////////
//  Reactance.cpp
//  Implementation of the Class Reactance
//  Created on:      28-Jan-2016 12:46:25
///////////////////////////////////////////////////////////

#include "Reactance.h"

using IEC61970::Base::Domain::Reactance;


Reactance::Reactance(){

}



Reactance::~Reactance(){

}


const IEC61970::Base::Domain::UnitSymbol Reactance::unit = IEC61970::Base::Domain::UnitSymbol::ohm;
