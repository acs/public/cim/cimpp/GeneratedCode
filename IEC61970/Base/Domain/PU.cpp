///////////////////////////////////////////////////////////
//  PU.cpp
//  Implementation of the Class PU
//  Created on:      28-Jan-2016 12:46:22
///////////////////////////////////////////////////////////

#include "PU.h"

using IEC61970::Base::Domain::PU;


PU::PU(){

}



PU::~PU(){

}


const IEC61970::Base::Domain::UnitMultiplier PU::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol PU::unit = IEC61970::Base::Domain::UnitSymbol::none;
