///////////////////////////////////////////////////////////
//  Inductance.cpp
//  Implementation of the Class Inductance
//  Created on:      28-Jan-2016 12:45:27
///////////////////////////////////////////////////////////

#include "Inductance.h"

using IEC61970::Base::Domain::Inductance;


Inductance::Inductance(){

}



Inductance::~Inductance(){

}


const IEC61970::Base::Domain::UnitSymbol Inductance::unit = IEC61970::Base::Domain::UnitSymbol::H;
