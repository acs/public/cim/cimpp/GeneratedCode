///////////////////////////////////////////////////////////
//  RealEnergy.cpp
//  Implementation of the Class RealEnergy
//  Created on:      28-Jan-2016 12:46:27
///////////////////////////////////////////////////////////

#include "RealEnergy.h"

using IEC61970::Base::Domain::RealEnergy;


RealEnergy::RealEnergy(){

}



RealEnergy::~RealEnergy(){

}


const IEC61970::Base::Domain::UnitSymbol RealEnergy::unit = IEC61970::Base::Domain::UnitSymbol::Wh;
