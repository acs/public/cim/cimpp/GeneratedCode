///////////////////////////////////////////////////////////
//  ActivePowerPerFrequency.cpp
//  Implementation of the Class ActivePowerPerFrequency
//  Created on:      28-Jan-2016 12:43:15
///////////////////////////////////////////////////////////

#include "ActivePowerPerFrequency.h"

using IEC61970::Base::Domain::ActivePowerPerFrequency;


ActivePowerPerFrequency::ActivePowerPerFrequency(){

}



ActivePowerPerFrequency::~ActivePowerPerFrequency(){

}


const IEC61970::Base::Domain::UnitSymbol ActivePowerPerFrequency::unit = IEC61970::Base::Domain::UnitSymbol::WPers;
