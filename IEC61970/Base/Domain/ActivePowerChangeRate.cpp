///////////////////////////////////////////////////////////
//  ActivePowerChangeRate.cpp
//  Implementation of the Class ActivePowerChangeRate
//  Created on:      28-Jan-2016 12:43:14
///////////////////////////////////////////////////////////

#include "ActivePowerChangeRate.h"

using IEC61970::Base::Domain::ActivePowerChangeRate;


ActivePowerChangeRate::ActivePowerChangeRate(){

}



ActivePowerChangeRate::~ActivePowerChangeRate(){

}


const IEC61970::Base::Domain::UnitSymbol ActivePowerChangeRate::unit = IEC61970::Base::Domain::UnitSymbol::WPers;
