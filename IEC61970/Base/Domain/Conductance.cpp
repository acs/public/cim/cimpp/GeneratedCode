///////////////////////////////////////////////////////////
//  Conductance.cpp
//  Implementation of the Class Conductance
//  Created on:      28-Jan-2016 12:43:36
///////////////////////////////////////////////////////////

#include "Conductance.h"

using IEC61970::Base::Domain::Conductance;


Conductance::Conductance(){

}



Conductance::~Conductance(){

}


const IEC61970::Base::Domain::UnitSymbol Conductance::unit = IEC61970::Base::Domain::UnitSymbol::S;
