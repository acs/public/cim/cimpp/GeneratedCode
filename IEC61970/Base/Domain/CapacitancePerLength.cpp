///////////////////////////////////////////////////////////
//  CapacitancePerLength.cpp
//  Implementation of the Class CapacitancePerLength
//  Created on:      28-Jan-2016 12:43:32
//  Original author: T. Kostic
///////////////////////////////////////////////////////////

#include "CapacitancePerLength.h"

using IEC61970::Base::Domain::CapacitancePerLength;


CapacitancePerLength::CapacitancePerLength(){

}



CapacitancePerLength::~CapacitancePerLength(){

}


const IEC61970::Base::Domain::UnitMultiplier CapacitancePerLength::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol CapacitancePerLength::unit = IEC61970::Base::Domain::UnitSymbol::F;
