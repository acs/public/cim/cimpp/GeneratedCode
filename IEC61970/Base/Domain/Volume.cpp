///////////////////////////////////////////////////////////
//  Volume.cpp
//  Implementation of the Class Volume
//  Created on:      28-Jan-2016 12:47:40
///////////////////////////////////////////////////////////

#include "Volume.h"

using IEC61970::Base::Domain::Volume;


Volume::Volume(){

}



Volume::~Volume(){

}


const IEC61970::Base::Domain::UnitSymbol Volume::unit = IEC61970::Base::Domain::UnitSymbol::m3;
