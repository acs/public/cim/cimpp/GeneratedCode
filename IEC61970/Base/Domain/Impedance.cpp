///////////////////////////////////////////////////////////
//  Impedance.cpp
//  Implementation of the Class Impedance
//  Created on:      28-Jan-2016 12:45:26
///////////////////////////////////////////////////////////

#include "Impedance.h"

using IEC61970::Base::Domain::Impedance;


Impedance::Impedance(){

}



Impedance::~Impedance(){

}


const IEC61970::Base::Domain::UnitSymbol Impedance::unit = IEC61970::Base::Domain::UnitSymbol::ohm;
