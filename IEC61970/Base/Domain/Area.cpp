///////////////////////////////////////////////////////////
//  Area.cpp
//  Implementation of the Class Area
//  Created on:      28-Jan-2016 12:43:22
//  Original author: SELAOST1
///////////////////////////////////////////////////////////

#include "Area.h"

using IEC61970::Base::Domain::Area;


Area::Area(){

}



Area::~Area(){

}


const IEC61970::Base::Domain::UnitSymbol Area::unit = IEC61970::Base::Domain::UnitSymbol::m2;
