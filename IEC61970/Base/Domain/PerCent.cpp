///////////////////////////////////////////////////////////
//  PerCent.cpp
//  Implementation of the Class PerCent
//  Created on:      28-Jan-2016 12:45:56
///////////////////////////////////////////////////////////

#include "PerCent.h"

using IEC61970::Base::Domain::PerCent;


PerCent::PerCent(){

}



PerCent::~PerCent(){

}


const IEC61970::Base::Domain::UnitMultiplier PerCent::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol PerCent::unit = IEC61970::Base::Domain::UnitSymbol::none;
