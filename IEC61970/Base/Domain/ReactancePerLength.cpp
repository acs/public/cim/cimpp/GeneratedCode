///////////////////////////////////////////////////////////
//  ReactancePerLength.cpp
//  Implementation of the Class ReactancePerLength
//  Created on:      28-Jan-2016 12:46:25
//  Original author: T. Kostic
///////////////////////////////////////////////////////////

#include "ReactancePerLength.h"

using IEC61970::Base::Domain::ReactancePerLength;


ReactancePerLength::ReactancePerLength(){

}



ReactancePerLength::~ReactancePerLength(){

}


const IEC61970::Base::Domain::UnitMultiplier ReactancePerLength::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol ReactancePerLength::unit = IEC61970::Base::Domain::UnitSymbol::ohmPerm;
