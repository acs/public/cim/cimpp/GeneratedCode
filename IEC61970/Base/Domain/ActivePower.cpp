///////////////////////////////////////////////////////////
//  ActivePower.cpp
//  Implementation of the Class ActivePower
//  Created on:      28-Jan-2016 12:43:14
///////////////////////////////////////////////////////////

#include "ActivePower.h"

using IEC61970::Base::Domain::ActivePower;


ActivePower::ActivePower(){

}



ActivePower::~ActivePower(){

}


const IEC61970::Base::Domain::UnitSymbol ActivePower::unit = IEC61970::Base::Domain::UnitSymbol::W;
