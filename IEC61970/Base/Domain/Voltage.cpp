///////////////////////////////////////////////////////////
//  Voltage.cpp
//  Implementation of the Class Voltage
//  Created on:      28-Jan-2016 12:47:34
///////////////////////////////////////////////////////////

#include "Voltage.h"

using IEC61970::Base::Domain::Voltage;


Voltage::Voltage(){

}



Voltage::~Voltage(){

}


const IEC61970::Base::Domain::UnitSymbol Voltage::unit = IEC61970::Base::Domain::UnitSymbol::V;
