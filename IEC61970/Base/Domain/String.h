///////////////////////////////////////////////////////////
//  String.h
//  Implementation of the Class String
//  Created on:      28-Jan-2016 12:46:49
///////////////////////////////////////////////////////////

#if !defined(EA_1947DDCB_05EE_45b1_9185_3D4E3335E142__INCLUDED_)
#define EA_1947DDCB_05EE_45b1_9185_3D4E3335E142__INCLUDED_

#include <string>

namespace IEC61970
{
	namespace Base
	{
		namespace Domain
		{
			/**
			 * A string consisting of a sequence of characters. The character encoding is UTF-
			 * 8. The string length is unspecified and unlimited.
			 */
			typedef std::string String;

		}

	}

}
#endif // !defined(EA_1947DDCB_05EE_45b1_9185_3D4E3335E142__INCLUDED_)
