///////////////////////////////////////////////////////////
//  Frequency.cpp
//  Implementation of the Class Frequency
//  Created on:      28-Jan-2016 12:44:54
///////////////////////////////////////////////////////////

#include "Frequency.h"

using IEC61970::Base::Domain::Frequency;


Frequency::Frequency(){

}



Frequency::~Frequency(){

}


const IEC61970::Base::Domain::UnitSymbol Frequency::unit = IEC61970::Base::Domain::UnitSymbol::Hz;
