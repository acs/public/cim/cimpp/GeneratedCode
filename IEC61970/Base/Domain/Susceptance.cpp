///////////////////////////////////////////////////////////
//  Susceptance.cpp
//  Implementation of the Class Susceptance
//  Created on:      28-Jan-2016 12:46:56
///////////////////////////////////////////////////////////

#include "Susceptance.h"

using IEC61970::Base::Domain::Susceptance;


Susceptance::Susceptance(){

}



Susceptance::~Susceptance(){

}


const IEC61970::Base::Domain::UnitSymbol Susceptance::unit = IEC61970::Base::Domain::UnitSymbol::S;
