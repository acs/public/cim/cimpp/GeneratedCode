///////////////////////////////////////////////////////////
//  Pressure.cpp
//  Implementation of the Class Pressure
//  Created on:      28-Jan-2016 12:46:11
///////////////////////////////////////////////////////////

#include "Pressure.h"

using IEC61970::Base::Domain::Pressure;


Pressure::Pressure(){

}



Pressure::~Pressure(){

}


const IEC61970::Base::Domain::UnitSymbol Pressure::unit = IEC61970::Base::Domain::UnitSymbol::Pa;
