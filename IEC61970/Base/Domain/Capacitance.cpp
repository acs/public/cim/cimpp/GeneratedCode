///////////////////////////////////////////////////////////
//  Capacitance.cpp
//  Implementation of the Class Capacitance
//  Created on:      28-Jan-2016 12:43:32
///////////////////////////////////////////////////////////

#include "Capacitance.h"

using IEC61970::Base::Domain::Capacitance;


Capacitance::Capacitance(){

}



Capacitance::~Capacitance(){

}


const IEC61970::Base::Domain::UnitSymbol Capacitance::unit = IEC61970::Base::Domain::UnitSymbol::F;
