///////////////////////////////////////////////////////////
//  ActivePowerPerCurrentFlow.cpp
//  Implementation of the Class ActivePowerPerCurrentFlow
//  Created on:      28-Jan-2016 12:43:15
//  Original author: 222206
///////////////////////////////////////////////////////////

#include "ActivePowerPerCurrentFlow.h"

using IEC61970::Base::Domain::ActivePowerPerCurrentFlow;


ActivePowerPerCurrentFlow::ActivePowerPerCurrentFlow(){

}



ActivePowerPerCurrentFlow::~ActivePowerPerCurrentFlow(){

}


const IEC61970::Base::Domain::UnitSymbol ActivePowerPerCurrentFlow::unit = IEC61970::Base::Domain::UnitSymbol::WPerA;
