///////////////////////////////////////////////////////////
//  ConductancePerLength.cpp
//  Implementation of the Class ConductancePerLength
//  Created on:      28-Jan-2016 12:43:36
//  Original author: T. Kostic
///////////////////////////////////////////////////////////

#include "ConductancePerLength.h"

using IEC61970::Base::Domain::ConductancePerLength;


ConductancePerLength::ConductancePerLength(){

}



ConductancePerLength::~ConductancePerLength(){

}


const IEC61970::Base::Domain::UnitMultiplier ConductancePerLength::multiplier = IEC61970::Base::Domain::UnitMultiplier::none;
const IEC61970::Base::Domain::UnitSymbol ConductancePerLength::unit = IEC61970::Base::Domain::UnitSymbol::SPerm;
