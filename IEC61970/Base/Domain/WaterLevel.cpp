///////////////////////////////////////////////////////////
//  WaterLevel.cpp
//  Implementation of the Class WaterLevel
//  Created on:      28-Jan-2016 12:47:43
///////////////////////////////////////////////////////////

#include "WaterLevel.h"

using IEC61970::Base::Domain::WaterLevel;


WaterLevel::WaterLevel(){

}



WaterLevel::~WaterLevel(){

}


const IEC61970::Base::Domain::UnitSymbol WaterLevel::unit = IEC61970::Base::Domain::UnitSymbol::m;
