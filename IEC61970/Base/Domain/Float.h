///////////////////////////////////////////////////////////
//  Float.h
//  Implementation of the Class Float
//  Created on:      28-Jan-2016 12:44:51
///////////////////////////////////////////////////////////

#if !defined(EA_79F22551_4058_4823_A0F3_51337F8E537F__INCLUDED_)
#define EA_79F22551_4058_4823_A0F3_51337F8E537F__INCLUDED_

namespace IEC61970
{
	namespace Base
	{
		namespace Domain
		{
			/**
			 * A floating point number. The range is unspecified and not limited.
			 */
			typedef double Float;

		}

	}

}
#endif // !defined(EA_79F22551_4058_4823_A0F3_51337F8E537F__INCLUDED_)
