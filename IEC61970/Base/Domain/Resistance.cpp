///////////////////////////////////////////////////////////
//  Resistance.cpp
//  Implementation of the Class Resistance
//  Created on:      28-Jan-2016 12:46:35
///////////////////////////////////////////////////////////

#include "Resistance.h"

using IEC61970::Base::Domain::Resistance;


Resistance::Resistance(){

}



Resistance::~Resistance(){

}


const IEC61970::Base::Domain::UnitSymbol Resistance::unit = IEC61970::Base::Domain::UnitSymbol::ohm;
