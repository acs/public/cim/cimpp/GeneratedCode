///////////////////////////////////////////////////////////
//  CurrentFlow.cpp
//  Implementation of the Class CurrentFlow
//  Created on:      28-Jan-2016 12:43:48
///////////////////////////////////////////////////////////

#include "CurrentFlow.h"

using IEC61970::Base::Domain::CurrentFlow;


CurrentFlow::CurrentFlow(){

}



CurrentFlow::~CurrentFlow(){

}


const IEC61970::Base::Domain::UnitSymbol CurrentFlow::unit = IEC61970::Base::Domain::UnitSymbol::A;
