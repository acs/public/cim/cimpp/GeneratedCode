///////////////////////////////////////////////////////////
//  VoltageAdjusterDynamics.cpp
//  Implementation of the Class VoltageAdjusterDynamics
//  Created on:      28-Jan-2016 12:47:35
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "VoltageAdjusterDynamics.h"

using IEC61970::Dynamics::StandardModels::VoltageAdjusterDynamics::VoltageAdjusterDynamics;


VoltageAdjusterDynamics::VoltageAdjusterDynamics()
	: PFVArControllerType1Dynamics(nullptr)
{

}



VoltageAdjusterDynamics::~VoltageAdjusterDynamics(){

}
