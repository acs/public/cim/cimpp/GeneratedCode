///////////////////////////////////////////////////////////
//  TurbineLoadControllerDynamics.cpp
//  Implementation of the Class TurbineLoadControllerDynamics
//  Created on:      28-Jan-2016 12:47:27
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "TurbineLoadControllerDynamics.h"

using IEC61970::Dynamics::StandardModels::TurbineLoadControllerDynamics::TurbineLoadControllerDynamics;


TurbineLoadControllerDynamics::TurbineLoadControllerDynamics()
	: TurbineGovernorDynamics(nullptr)
{

}



TurbineLoadControllerDynamics::~TurbineLoadControllerDynamics(){

}
