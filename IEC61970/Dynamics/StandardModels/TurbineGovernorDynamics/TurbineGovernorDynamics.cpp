///////////////////////////////////////////////////////////
//  TurbineGovernorDynamics.cpp
//  Implementation of the Class TurbineGovernorDynamics
//  Created on:      28-Jan-2016 12:47:26
//  Original author: tsaxton
///////////////////////////////////////////////////////////

#include "TurbineGovernorDynamics.h"

using IEC61970::Dynamics::StandardModels::TurbineGovernorDynamics::TurbineGovernorDynamics;


TurbineGovernorDynamics::TurbineGovernorDynamics()
	: AsynchronousMachineDynamics(nullptr)
{

}



TurbineGovernorDynamics::~TurbineGovernorDynamics(){

}
