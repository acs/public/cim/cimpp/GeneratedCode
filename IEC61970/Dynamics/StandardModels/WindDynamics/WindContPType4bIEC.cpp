///////////////////////////////////////////////////////////
//  WindContPType4bIEC.cpp
//  Implementation of the Class WindContPType4bIEC
//  Created on:      28-Jan-2016 12:47:48
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindContPType4bIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindContPType4bIEC;


WindContPType4bIEC::WindContPType4bIEC()
	: WindTurbineType4bIEC(nullptr)
{

}



WindContPType4bIEC::~WindContPType4bIEC(){

}
