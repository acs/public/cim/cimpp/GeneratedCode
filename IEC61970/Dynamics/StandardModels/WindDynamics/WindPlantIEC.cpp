///////////////////////////////////////////////////////////
//  WindPlantIEC.cpp
//  Implementation of the Class WindPlantIEC
//  Created on:      28-Jan-2016 12:47:57
//  Original author: civanov
///////////////////////////////////////////////////////////

#include "WindPlantIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindPlantIEC;


WindPlantIEC::WindPlantIEC()
	: WindPlantReactiveControlIEC(nullptr)
{

}



WindPlantIEC::~WindPlantIEC(){

}
