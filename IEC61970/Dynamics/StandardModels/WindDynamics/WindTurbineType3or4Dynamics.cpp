///////////////////////////////////////////////////////////
//  WindTurbineType3or4Dynamics.cpp
//  Implementation of the Class WindTurbineType3or4Dynamics
//  Created on:      28-Jan-2016 12:48:00
//  Original author: civanov
///////////////////////////////////////////////////////////

#include "WindTurbineType3or4Dynamics.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindTurbineType3or4Dynamics;


WindTurbineType3or4Dynamics::WindTurbineType3or4Dynamics()
	: RemoteInputSignal(nullptr), WindTurbineType3or4(nullptr)
{

}



WindTurbineType3or4Dynamics::~WindTurbineType3or4Dynamics(){

}
