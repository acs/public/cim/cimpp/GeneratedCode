///////////////////////////////////////////////////////////
//  WindContPType3IEC.cpp
//  Implementation of the Class WindContPType3IEC
//  Created on:      28-Jan-2016 12:47:48
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindContPType3IEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindContPType3IEC;


WindContPType3IEC::WindContPType3IEC()
	: WindGenTurbineType3IEC(nullptr)
{

}



WindContPType3IEC::~WindContPType3IEC(){

}
