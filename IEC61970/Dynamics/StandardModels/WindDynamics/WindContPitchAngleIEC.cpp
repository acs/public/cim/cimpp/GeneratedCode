///////////////////////////////////////////////////////////
//  WindContPitchAngleIEC.cpp
//  Implementation of the Class WindContPitchAngleIEC
//  Created on:      28-Jan-2016 12:47:47
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindContPitchAngleIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindContPitchAngleIEC;


WindContPitchAngleIEC::WindContPitchAngleIEC()
	: WindGenTurbineType3IEC(nullptr)
{

}



WindContPitchAngleIEC::~WindContPitchAngleIEC(){

}
