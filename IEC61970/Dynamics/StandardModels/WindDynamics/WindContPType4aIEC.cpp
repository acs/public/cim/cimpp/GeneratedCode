///////////////////////////////////////////////////////////
//  WindContPType4aIEC.cpp
//  Implementation of the Class WindContPType4aIEC
//  Created on:      28-Jan-2016 12:47:48
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindContPType4aIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindContPType4aIEC;


WindContPType4aIEC::WindContPType4aIEC()
	: WindTurbineType4aIEC(nullptr)
{

}



WindContPType4aIEC::~WindContPType4aIEC(){

}
