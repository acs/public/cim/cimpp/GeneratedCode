///////////////////////////////////////////////////////////
//  WindAeroLinearIEC.cpp
//  Implementation of the Class WindAeroLinearIEC
//  Created on:      28-Jan-2016 12:47:46
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindAeroLinearIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindAeroLinearIEC;


WindAeroLinearIEC::WindAeroLinearIEC()
	: WindGenTurbineType3IEC(nullptr)
{

}



WindAeroLinearIEC::~WindAeroLinearIEC(){

}
