///////////////////////////////////////////////////////////
//  WindPlantFreqPcontrolIEC.cpp
//  Implementation of the Class WindPlantFreqPcontrolIEC
//  Created on:      28-Jan-2016 12:47:56
//  Original author: civanov
///////////////////////////////////////////////////////////

#include "WindPlantFreqPcontrolIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindPlantFreqPcontrolIEC;


WindPlantFreqPcontrolIEC::WindPlantFreqPcontrolIEC()
	: WindPlantIEC(nullptr)
{

}



WindPlantFreqPcontrolIEC::~WindPlantFreqPcontrolIEC(){

}
