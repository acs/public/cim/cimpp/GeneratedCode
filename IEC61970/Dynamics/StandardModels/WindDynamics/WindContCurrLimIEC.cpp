///////////////////////////////////////////////////////////
//  WindContCurrLimIEC.cpp
//  Implementation of the Class WindContCurrLimIEC
//  Created on:      28-Jan-2016 12:47:46
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "WindContCurrLimIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindContCurrLimIEC;


WindContCurrLimIEC::WindContCurrLimIEC()
	: WindTurbineType3or4IEC(nullptr)
{

}



WindContCurrLimIEC::~WindContCurrLimIEC(){

}
