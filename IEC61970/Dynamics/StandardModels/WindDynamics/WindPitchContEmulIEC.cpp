///////////////////////////////////////////////////////////
//  WindPitchContEmulIEC.cpp
//  Implementation of the Class WindPitchContEmulIEC
//  Created on:      28-Jan-2016 12:47:55
//  Original author: civanov
///////////////////////////////////////////////////////////

#include "WindPitchContEmulIEC.h"

using IEC61970::Dynamics::StandardModels::WindDynamics::WindPitchContEmulIEC;


WindPitchContEmulIEC::WindPitchContEmulIEC()
	: WindGenTurbineType2IEC(nullptr)
{

}



WindPitchContEmulIEC::~WindPitchContEmulIEC(){

}
