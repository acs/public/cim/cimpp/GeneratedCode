///////////////////////////////////////////////////////////
//  PowerSystemStabilizerDynamics.cpp
//  Implementation of the Class PowerSystemStabilizerDynamics
//  Created on:      28-Jan-2016 12:46:09
//  Original author: tsaxton
///////////////////////////////////////////////////////////

#include "PowerSystemStabilizerDynamics.h"

using IEC61970::Dynamics::StandardModels::PowerSystemStabilizerDynamics::PowerSystemStabilizerDynamics;


PowerSystemStabilizerDynamics::PowerSystemStabilizerDynamics()
	: ExcitationSystemDynamics(nullptr)
{

}



PowerSystemStabilizerDynamics::~PowerSystemStabilizerDynamics(){

}
