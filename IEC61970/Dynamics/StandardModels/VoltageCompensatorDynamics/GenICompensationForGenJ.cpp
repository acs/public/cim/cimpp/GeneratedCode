///////////////////////////////////////////////////////////
//  GenICompensationForGenJ.cpp
//  Implementation of the Class GenICompensationForGenJ
//  Created on:      28-Jan-2016 12:44:57
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "GenICompensationForGenJ.h"

using IEC61970::Dynamics::StandardModels::VoltageCompensatorDynamics::GenICompensationForGenJ;


GenICompensationForGenJ::GenICompensationForGenJ()
	: VcompIEEEType2(nullptr), SynchronousMachineDynamics(nullptr)
{

}



GenICompensationForGenJ::~GenICompensationForGenJ(){

}
