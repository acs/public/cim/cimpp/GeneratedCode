///////////////////////////////////////////////////////////
//  OverexcitationLimiterDynamics.cpp
//  Implementation of the Class OverexcitationLimiterDynamics
//  Created on:      28-Jan-2016 12:45:53
//  Original author: ppbr003
///////////////////////////////////////////////////////////

#include "OverexcitationLimiterDynamics.h"

using IEC61970::Dynamics::StandardModels::OverexcitationLimiterDynamics::OverexcitationLimiterDynamics;


OverexcitationLimiterDynamics::OverexcitationLimiterDynamics()
	: ExcitationSystemDynamics(nullptr)
{

}



OverexcitationLimiterDynamics::~OverexcitationLimiterDynamics(){

}
