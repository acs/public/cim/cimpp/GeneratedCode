///////////////////////////////////////////////////////////
//  AsynchronousMachineDynamics.cpp
//  Implementation of the Class AsynchronousMachineDynamics
//  Created on:      28-Jan-2016 12:43:23
//  Original author: tsaxton
///////////////////////////////////////////////////////////

#include "AsynchronousMachineDynamics.h"

using IEC61970::Dynamics::StandardModels::AsynchronousMachineDynamics::AsynchronousMachineDynamics;


AsynchronousMachineDynamics::AsynchronousMachineDynamics()
	: AsynchronousMachine(nullptr)
{

}



AsynchronousMachineDynamics::~AsynchronousMachineDynamics(){

}
