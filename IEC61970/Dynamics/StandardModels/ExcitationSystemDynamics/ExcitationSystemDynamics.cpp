///////////////////////////////////////////////////////////
//  ExcitationSystemDynamics.cpp
//  Implementation of the Class ExcitationSystemDynamics
//  Created on:      28-Jan-2016 12:44:39
//  Original author: tsaxton
///////////////////////////////////////////////////////////

#include "ExcitationSystemDynamics.h"

using IEC61970::Dynamics::StandardModels::ExcitationSystemDynamics::ExcitationSystemDynamics;


ExcitationSystemDynamics::ExcitationSystemDynamics()
	: SynchronousMachineDynamics(nullptr)
{

}



ExcitationSystemDynamics::~ExcitationSystemDynamics(){

}
